Title: Job Offers
menulabel: Offers
Date: 2022-09-24
Slug: Offres
lang: en
sortorder: 05

# phD Thesis (2023)

* Turbulence diphasique : [Transport and dispersion of a cluster of particles by a vortex structure]({filename}../BIBLIO/PhD_IMFT_AlbagnacViroulet_2023-2026.pdf) ([ALBAGNAC Julie & VIROULET Sylvain]), Institut de Mécanique des Fluides de Toulouse)

# M2 internship (2023)

* [Turbulence en ingénierie automobile : ]({filename}../stages_M2/JK_LES_aeroacoustics.md) Large Eddy Simulation of the flow around a prototypical car and aeroacoustics interaction ([J. Kreuzinger] https://www.km-turbulenz.de), KM-Turbulenz GmbH, Munich, Germany)

* [Turbulence en ingénierie hydraulique : ]({filename}../stages_M2/SL_cfd_turbine.md) Investigation of the Deep Part Load Behavior of a Francis Turbine via CFD ([S. Leguizamon](https://www.researchgate.net/profile/Sebastian-Leguizamon), GE Renewable Energy Grenoble)

* [Turbulence et méthodes numériques: ]({filename}../stages_M2/GB_turb_stabilite_num.md) Investigation of the stability limits of a linearized implicit scheme for turbulent incompressible flows ([G. Balarac](https://www.legi.grenoble-inp.fr/web/spip.php?auteur57), LEGI Grenoble)

* [Turbulence synthétique et modélisation numérique : ]({filename}../stages_M2/CF_turb_synthetique.md) Vers un forçage pour la turbulence synthétique inhomogène à divergence et hélicité nulles. ([C. Friess](https://www.m2p2.fr/annuaire-304/christophe-friess-133.htm), LM2P2 Marseille)

* [Turbulence atmosphérique et ondes de gravité : ]({filename}../stages_M2/CL_bore_gravite.md)  Étude du coup de vent du 18 juin 2022 sur la Normandie à l’aide de simulations et d’observations à haute résolution ([C. Lac](https://www.umr-cnrm.fr/spip.php?article1220), CNRM Toulouse)

* [Turbulence en ingénierie de l’environnement : ]({filename}../stages_M2/JC_sediment.md) Numerical analysis of flow and morphological patterns around moving objects ([E. Puig Montella](https://www.legi.grenoble-inp.fr/web/spip.php?auteur397), LEGI Grenoble)

* [Turbulence en ingénierie de l’environnement : ]({filename}../stages_M2/JC_hydraulic.md) Numerical simulation of air-entrainment in plunging jets and hydraulic jumps ([J. Chauchat](http://www.legi.grenoble-inp.fr/people/Julien.Chauchat/), LEGI Grenoble)

* [Turbulence superfluide : ]({filename}../stages_M2/PD_particle_helium.md) 3D Particle tracking for the study of superfluid 4He turbulence. (P. Diribarne, [CEA/DSBT](https://www.d-sbt.fr/Pages/Presentation.aspx) Grenoble)

* [Instrumentation en turbulence à haut nombre de Reynolds : ]({filename}../stages_M2/AG_hw_anemo.md) Hot wire anemometry in high Reynolds turbulent flows. (A. Girard, [CEA/DSBT](https://www.d-sbt.fr/Pages/Presentation.aspx) Grenoble)

* [Two-phase flow turbulence : ]({filename}../stages_M2/NM_atomization2.md) The role of gas turbulence in gas-assisted liquid atomization and sprays ([N. Machicoane](http://www.legi.grenoble-inp.fr/people/Nathanael.Machicoane/), LEGI Grenoble)

* [Turbulence & topography : ]({filename}../stages_M2/DC_topo_turb.md) Towards the ocean and planetary cores ([D. Cebron](https://www.isterre.fr/annuaire/pages-web-du-personnel/david-cebron/), ISTerre Grenoble)

* [Turbulence & magnetic field ]({filename}../stages_M2/NS_magnetic_tidal.md) generation by tidal forcing in internal liquid planetary layers ([N. Schaeffer](https://www.isterre.fr/annuaire/pages-web-du-personnel/nathanael-schaeffer/), ISTerre Grenoble)

# M1 internship (2022)

* [Stratified turbulence]({filename}../stages_M1/PA_strat_turb.md)
Spatio-temporal analysis of strongly stratified turbulence forced in rotational
modes ([P. Augier](http://www.legi.grenoble-inp.fr/people/Pierre.Augier/), LEGI
Grenoble)

* [Transferts thermiques et turbulents : ]({filename}../stages_M1/OB_energetique.md) Etude expérimentale de l’intensification des transferts thermiques par ultrasons ([O. Bulliard-Sauret](https://www.researchgate.net/profile/Odin-Bulliard-Sauret), LEGI Grenoble)

* [In lab turbulent geophysical flows : ]({filename}../stages_M1/EN_ravity_currents.md) Data analysis of experiments on rotating downslope gravity currents ([E. Negretti](https://www.legi.grenoble-inp.fr/web/spip.php?auteur227), LEGI Grenoble)

* [Turbulence in wind tunnel : ]({filename}../stages_M1/MO_active_grid.md) The turbulence properties of active-grid-generated flows ([M. Obligado](http://www.legi.grenoble-inp.fr/web/spip.php?auteur77), LEGI Grenoble)

* [Numerical modeling of turbulence in planetary core : ]({filename}../stages_M1/DC_lunar_core.md) Turbulent boundary stress in the lunar core ([D. Cebron](https://www.isterre.fr/annuaire/pages-web-du-personnel/david-cebron/), ISTerre Grenoble)

* [Two-phase flow turbulence : ]({filename}../stages_M1/NM_spray.md) The role of gas turbulence in gas-assisted liquid atomization and sprays ([N. Machicoane](http://www.legi.grenoble-inp.fr/people/Nathanael.Machicoane/), LEGI Grenoble)

* [Analyse de données in situ de turbulence : ]({filename}../stages_M1/CB_antarctique.md) Turbulence dans les vents catabatiques en Antarctique ([C. Brun](http://www.legi.grenoble-inp.fr/web/spip.php?auteur41), LEGI Grenoble)

* [Turbulence optique : ]({filename}../stages_M1/JMC_scintillometer.md) In situ measurements with a scintillometer ([J.M. Cohard](http://www.ige-grenoble.fr/-jean-martial-cohard-), IGE Grenoble)


# postdoc

# CDD/CDI
