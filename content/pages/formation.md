Title: Formation M2 TMA
menulabel: Formation
Date: 2021-01-17
Category: 4. Formation
Slug: formation
lang: fr
sortorder: 03

# Objectif de la formation

Il s’agit de former des experts scientifiques qui maitrisent de façon exhaustive l’ensemble des méthodes et outils numériques, expérimentaux et théoriques dans un large domaine d’applications de la turbulence, des écoulements industriels à l’astrophysique, en passant par les géosciences (atmosphère, océans, rivières), l’environnement (météorologie, qualité de l’air), l’aéronautique, l’énergie et le transport. Les étudiants qui choisiront ce parcours souhaiteront devenir experts en mécanique des fluides et turbulence avant de s’orienter vers une application dans un domaine spécifique. Le M2 TMA propose une approche pédagogique inovante en recentrant d’abord l’enseignement sur la discipline scientifique, en l’occurrence la turbulence, et toutes les voies d’approche permettant son analyse, avec une vision interdisciplinaire unique.

# Contenu de la formation

## M2-semestre 1 : 30 ECTS

### modules turbulence et processus : 15 ECTS

* [Physique théorique de la turbulence]({filename}../modules/physique_theo_turb.md) 3 ECTS ([L. Canet](https://lpmmc.cnrs.fr/spip.php?auteur9))
* [Ecoulements diphasiques turbulents]({filename}../modules/ecoulement_diph_turb.md) 3 ECTS ([M. Obligado](http://www.legi.grenoble-inp.fr/web/spip.php?auteur77), [P. sechet](https://www.legi.grenoble-inp.fr/web/spip.php?auteur163), [G. Balarac](https://www.legi.grenoble-inp.fr/web/spip.php?auteur57), [N. Machicoane](http://www.legi.grenoble-inp.fr/people/Nathanael.Machicoane/))
* [Turbulence compressible]({filename}../modules/turb_comp.md) 3 ECTS ([C. Brun](http://www.legi.grenoble-inp.fr/web/spip.php?auteur41), [Pierre Hily-Blant](https://ipag.osug.fr/~hilyblap/index.html#outline-container-orge4b8d74))
* [Effet dynamo et rotation en turbulence]({filename}../modules/dynamo_rotation_turb.md) 3 ECTS ([R. Deguen](https://www.isterre.fr/annuaire/pages-web-du-personnel/renaud-deguen/), [N. Schaeffer](https://www.isterre.fr/annuaire/pages-web-du-personnel/nathanael-schaeffer/), [D. Cebron](https://www.isterre.fr/annuaire/pages-web-du-personnel/david-cebron/))
* [Turbulence d'ondes]({filename}../modules/n_l_ondes.md) 3 ECTS ([N. Mordant](http://nicolas.mordant.free.fr/),
[P. Augier](http://www.legi.grenoble-inp.fr/people/Pierre.Augier/), [M. Gibert](https://neel.cnrs.fr/les-chercheurs-et-techniciens/mathieu-gibert))

### modules d’approfondissement : 9 ECTS

* [Méthodes expérimentales avancées]({filename}../modules/methode_exp.md) 3 ECTS: TP de turbulence (soufflerie aérodynamique, plaque tournante Coriolis, canal à houle, mesures en terrain réel...) ([M. Gibert](https://neel.cnrs.fr/les-chercheurs-et-techniciens/mathieu-gibert), [M. Obligado](http://www.legi.grenoble-inp.fr/web/spip.php?auteur77), [E. Negretti](https://www.legi.grenoble-inp.fr/web/spip.php?auteur227), [C. Brun](http://www.legi.grenoble-inp.fr/web/spip.php?auteur41), [H. Michalet](https://www.legi.grenoble-inp.fr/web/spip.php?auteur95), [N. Mordant](http://nicolas.mordant.free.fr/),
[H. Djeridi](https://www.legi.grenoble-inp.fr/web/spip.php?auteur155))
* [Méthodes numériques avancées]({filename}../modules/methode_num.md) 3 ECTS: HPC for Navier-Stokes equations (DNS, RANS, LES) ([G. Balarac](https://www.legi.grenoble-inp.fr/web/spip.php?auteur57), [C. Picard](https://christophe.picard.pages.ensimag.fr), [P. Augier](http://www.legi.grenoble-inp.fr/people/Pierre.Augier/))
* [Bilinguisme Compréhension anglais/français]({filename}../modules/bilinguisme.md) 3 ECTS: projet bibliographique & séminaires (Univ Poitiers: P. Moffatt, SDL : S. Perraud, CUEF FLE : A. Braibant)

### modules turbulence et applications : 6 ECTS  (1 ou 2 modules au choix)

* Aérodynamique 3 ECTS : [Contrôle et turbulence de paroi]({filename}../modules/controle.md) [M2 TMA](https://brunch.gricad-pages.univ-grenoble-alpes.fr/master-tma-turbulences-methodes-et-applications/) (ouverture conditionnelle 2023) ([S. Tardu](https://www.legi.grenoble-inp.fr/web/spip.php?auteur63))
* Mathématiques appliquées 6 ECTS: [GPU for Mathematical Models](https://msiam.imag.fr/lectures#gpu_computing) [M2 MSIAM](https://msiam.imag.fr) ([C. Picard](https://christophe.picard.pages.ensimag.fr), [M. Ismail](https://www-liphy.univ-grenoble-alpes.fr/pagesperso/ismail/))
* Astrophysique 3 ECTS: [Dynamique des plasmas astrophysiques](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-physique-IAQK7WZY/parcours-astrophysique-2e-annee-IAY1JK2G/ue-dynamique-des-plasmas-astrophysiques-IG0TWOCL.html) [M2 Astrophysique](https://master-physique.univ-grenoble-alpes.fr/le-programme/master-astro/) ([J. Ferreira](https://ipag.osug.fr/~ferreirj), [B. Cerutti](https://ipag.osug.fr/~ceruttbe/index.html),
[G. Lesur](https://ipag.osug.fr/~lesurg/))
* Intelligence Artificielle 0 ECTS (libre) : Introduction au Deep Learning [formation CNRS/MIAI](https://gricad-gitlab.univ-grenoble-alpes.fr/talks/fidle/-/wikis/Fidle%20à%20distance/Présentation) ([J.-L. Parouty ](https://simap.grenoble-inp.fr/en/research/jean-luc-parouty))
* Géophysique 6 (3+3) ECTS : [Dynamique des fluides géophysiques](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-sciences-de-la-terre-et-des-planetes-environnement-IAQK6K1B/parcours-systeme-climatique-atmosphere-hydrosphere-cryosphere-1re-et-2e-annees-KVQRM1RZ/ue-dynamique-des-fluides-geophysiques-geophysical-fluid-dynamics-KYVLV5F5.html) [M2 STPE](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-sciences-de-la-terre-et-des-planetes-environnement-IAQK6K1B/parcours-systeme-climatique-atmosphere-hydrosphere-cryosphere-1re-et-2e-annees-KVQRM1RZ.html) ([R. Deguen](https://www.isterre.fr/annuaire/pages-web-du-personnel/renaud-deguen/), [D. Cebron](https://www.isterre.fr/annuaire/pages-web-du-personnel/david-cebron/))
* Océan 3 ECTS: [data assimilation in geosciences]({filename}../modules/assimil.md) [M2 STPE](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-sciences-de-la-terre-et-des-planetes-environnement-IAQK6K1B/parcours-systeme-climatique-atmosphere-hydrosphere-cryosphere-1re-et-2e-annees-KVQRM1RZ.html) ([E. Cosme](http://pp.ige-grenoble.fr/pageperso/cosmee/))
* Atmosphère 3 ECTS: [Turbulence en couche limite atmosphérique]({filename}../modules/turb_cla.md) [M2 TMA](https://brunch.gricad-pages.univ-grenoble-alpes.fr/master-tma-turbulences-methodes-et-applications/) (ouverture conditionnelle 2023) ([C. Brun](http://www.legi.grenoble-inp.fr/web/spip.php?auteur41), [J.M. Cohard](http://www.ige-grenoble.fr/-jean-martial-cohard-))
* Environnement 6 (3+3) ECTS :  [Simulation en ingénierie de l’environnement](https://ense3.grenoble-inp.fr/fr/formation/outils-de-simulation-avanc-eacute-s-pour-la-m-eacute-canique-5eus5osa) [3A E3 filière HOE](https://ense3.grenoble-inp.fr/fr/formation/ingenieur-de-grenoble-inp-ense3-filiere-hydraulique-ouvrages-et-environnement-hoe#page-presentation) ([J. Chauchat](http://www.legi.grenoble-inp.fr/people/Julien.Chauchat/))
* Génie des Procédés 6 (3+3) ECTS: [transfert de chaleur](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-genie-des-procedes-et-des-bio-procedes-IAQKNFHK/parcours-genie-des-procedes-pour-l-energie-IBAWJJZ6/ue-transfert-de-chaleur-IG7XKUEZ.html) [M2 GDP pour l'Energie](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-genie-des-procedes-et-des-bio-procedes-IAQKNFHK/parcours-genie-des-procedes-pour-l-energie-IBAWJJZ6.html) ([O. Bulliard-Sauret](http://www.legi.grenoble-inp.fr/web/spip.php?auteur239))
* Machine learning in Earth Sciences 3 ECTS : [Introduction](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-sciences-de-la-terre-et-des-planetes-environnement-IAQK6K1B/parcours-systeme-climatique-atmosphere-hydrosphere-cryosphere-1re-et-2e-annees-KVQRM1RZ/ue-introduction-to-machine-learning-in-earth-sciences-KZ5PF1RS.html) [M2 STPE](https://formations.univ-grenoble-alpes.fr/fr/catalogue-2021/master-XB/master-sciences-de-la-terre-et-des-planetes-environnement-IAQK6K1B/parcours-systeme-climatique-atmosphere-hydrosphere-cryosphere-1re-et-2e-annees-KVQRM1RZ.html)  ([L. Moreau](https://www.isterre.fr/annuaire/pages-web-du-personnel/ludovic-moreau/article/publications.html), [E. Cosme](http://pp.ige-grenoble.fr/pageperso/cosmee/))


## M2-semestre 2 : 30 ECTS
* [stage M2 Recherche ou R&D]({filename}../offres_postes.md) 5 mois (30 ou 24 ECTS)
* Stage M1 en laboratoire 2 mois (6 ECTS) optionnel
