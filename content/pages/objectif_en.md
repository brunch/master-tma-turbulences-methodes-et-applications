Title: Objectives
Date: 2021-01-26
Slug: objectif
lang: en
sortorder: 00
URL:
save_as: index.html

The M2 TMA (Turbulence: Methods and Applications) master's offers a new and original training program on a complex and essential scientific topic. The pedagogical approach is innovative in that it refocuses the teaching on the scientific discipline, in this case turbulence. Straddling three majors (Physics, Applied Mathematics and Mechanics), the TMA course focuses on all avenues of analysis of turbulence with a unique interdisciplinary vision: fluid mechanics, mathematics, internal and external geophysics, physics, astrophysics and chemistry. Specialisation will be achieved at the end of the course through a choice of application modules and a 5-month M2 internship in a research laboratory or R&D research centre. Students who choose this course will want to become experts in fluid mechanics and turbulence before moving on to an application in a specific field.

![Image]({attach}../films/qinc_jet.gif)
_Numerical Simulation of a turbulent round jet. courtesy C.B._

![Image]({attach}../figures/jet_rond.png)
_Numerical Simulation of a turbulent coaxial. courtesy G.B._
