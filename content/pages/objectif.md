Title: Objectif
Date: 2021-01-26
Slug: objectif
lang: fr
sortorder: 00
URL:
save_as: index.html

Le parcours master M2 TMA (Turbulences : Méthodes et Applications) propose une
nouvelle formation originale sur une thématique scientifique complexe et
essentielle. L'approche pédagogique est innovante en recentrant d’abord
l’enseignement sur la discipline scientifique, en l’occurrence la turbulence. A
cheval sur 3 mentions (Physique, Mathématiques et applications, Mécanique) le
parcours TMA s'intéresse à toutes les voies d’analyse de la turbulence avec une
vision interdisciplinaire unique : mécanique des fluides, mathématiques,
géophysique interne et externe, physique, astrophysique, chimie. La
spécialisation se fera en fin de formation par le biais de modules applicatifs
au choix et par un stage de M2 de 5 mois en laboratoire de recherche ou centre
de recherche R&D. Les étudiants qui choisiront ce parcours souhaiteront devenir
experts en mécanique des fluides et turbulence avant de s’orienter vers une
application dans un domaine spécifique.

![Image]({attach}../films/qinc_jet.gif)
_Simulation numérique de jet rond turbulent. courtesy C.B._

![Image]({attach}../figures/jet_rond.png)
_Simulation numérique de jet coaxial turbulent. courtesy G.B._
