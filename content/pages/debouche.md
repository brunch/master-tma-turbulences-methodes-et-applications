Title: Débouchés des diplômés
menulabel: Débouchés
Date: 2021-01-17
Slug: debouches
lang: fr
sortorder: 04

Il s’agit de former des experts scientifiques qui maitrisent de façon exhaustive l’ensemble des méthodes et outils numériques, expérimentaux et théoriques dans un large domaine d’applications de la turbulence, des écoulements industriels à l’astrophysique, en passant par les géosciences (atmosphère, océans, rivières), l’environnement (météorologie, qualité de l’air), l’aéronautique, l’énergie et le transport. Les diplômés seront aptes à continuer des études de doctorat dans les laboratoires grenoblois spécialisés dans des thématiques pour lesquelles la turbulence joue un rôle majeur ou dans des centres d’étude publics grenoblois ou nationaux. Certains des diplômés pourront également se tourner vers des postes d’ingénieur de recherche dans les grands groupes privés développant des activités R&D reconnues.

Doctorat en laboratoire UGA  :
==============================
* [LEGI ](https://www.legi.grenoble-inp.fr/web/): équipes MoST, EDT, MEIGE, Energétique
* [LJK](https://www-ljk.imag.fr): mathématiques appliquées
* [ISTERRE](https://www.isterre.fr): géophysique interne (noyau terrestre)
* [IGE](https://www.ige-grenoble.fr): géophysique externe (océan, atmosphère, rivières)
* [LPMMC](https://lpmmc.cnrs.fr): physique des milieux condensés
* [Liphy](https://www-liphy.univ-grenoble-alpes.fr): physique interdisciplinaire
* [IPAG](https://ipag.osug.fr): astrophysique
* [SIMAP](https://simap.grenoble-inp.fr): génie des procédés

Doctorat ou R&D en centre de recherche Public  :
================================================
* [CEA](https://www.d-sbt.fr/Pages/Presentation.aspx): SBT cryogénie
* [Institut NEEL](https://neel.cnrs.fr/equipes-poles-et-services/helium-du-fondamental-aux-applications-helfa) : physique et turbulence dans l'Helium
* [CEN](https://www.umr-cnrm.fr) : montagne
* [INRAE](https://www6.lyon-grenoble.inrae.fr/etna/) : environnement et risques
* [CNRM](https://www.umr-cnrm.fr) : météo-France
* [ONERA](https://www.onera.fr/en/centers/chatillon) : aéronautique
* [EDF](https://www.edf.fr/groupe-edf/inventer-l-avenir-de-l-energie/r-d-un-savoir-faire-mondial/nos-offres/codes-de-calcul/code-saturne) : énergie et hydraulique

R&D en centre de recherche Privé  :
===================================
* PSA, Renault: aérodynamique et aéro-acoustique automobile
* Air liquide : lanceur spatial Ariane 6
* Safran : Aircraft Engines (SAE : Snecma) & Helicopter Engine (SHE : Turbomeca)
* General Electric (GE) : énergie hydraulique
* Compagnie Nationale du Rhône (CNR) : énergie et hydraulique
* Artellia: hydraulique environnementale
