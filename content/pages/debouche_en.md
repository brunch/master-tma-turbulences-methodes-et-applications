Title: Opportunities for graduates
menulabel: Opportunities
Date: 2021-01-17
Slug: debouches
lang: en
sortorder: 04

The aim is to train scientific experts who have an exhaustive understanding of all the numerical, experimental and theoretical methods and tools in a wide range of turbulence applications, from industrial flows to astrophysics, including geosciences (atmosphere, oceans, rivers), the environment (meteorology, air quality), aeronautics, energy and transport. Graduates will be able to pursue doctoral studies in Grenoble laboratories specialising in topics in which turbulence plays a major role or in Grenoble or national public research centres. Some of the graduates will also be able to take up positions as research engineers in large private groups developing recognised R&D activities.

PhD in a UGA laboratory :
==============================
* [LEGI ](https://www.legi.grenoble-inp.fr/web/): équipes MoST, EDT, MEIGE, Energétique
* LJK : applied mathematics
* ISTERRE : internal geophysics (earth core)
* IGE : external geophysics (ocean, atmosphere, rivers)
* LPMMC: physics of condensed media
* Liphy: interdisciplinary physics
* IPAG: Astrophysics
* SIMAP: process engineering

PhD or R&D in a Public Research centre :
================================================
* CEA : SBT cryogenics
* NEEL Institute: physics and turbulence
* CEN: mountains
* INRAE: environment and risks
* CNRM: meteo-France
* ONERA: aeronautics
* EDF: energy and hydraulics

R&D in a private research centre  :
===================================
* PSA, Renault: aerodynamics and automotive aeroacoustics
* Air Liquide: Ariane 6 space launcher
* Safran: Aircraft Engines (SAE: Snecma) & Helicopter Engine (SHE: Turbomeca)
* General Electric (GE): hydropower
* Compagnie Nationale du Rhône (CNR): energy and hydraulics
* Artellia: environmental hydraulics
