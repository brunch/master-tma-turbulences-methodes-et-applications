Title: Turbulent atmospheric boundary layer 3ECTS (24h)
Date: 2021-11-25
Category:  Teaching modules
lang: en

Christophe Brun, Jean-Martial Cohard

![Image]({static}../figures/ballon.png)
_courtesy C.B. BLM publication_

#Abstract :

Training in the basics of turbulence in the atmospheric boundary layer.


#Distribution CM/TD:
About 4 CM (6h), 4 TD (6h), 6 CM/TD (9h) and 1 TP (3h).

#Content :

##Part 1: Turbulence in the atmospheric boundary layer (under construction)

1. Turbulence
    * turbulent kinetic energy balance TKE and turbulent potential energy balance TPE
    * balance of momentum flux and sensible heat flux
    * anisotropy of turbulence
    * energy spectra

2. Neutral, stable and unstable CLA
    * thermal stratification
    * buoyancy and mechanical shear balance
    * Obukhov length scale
    * correction of the logarithmic law of velocity and temperature



##Part 2 : Optic Turbulence  (under construction)
1. Electromagnetic wave propagation in random media
    * Structure parameter in the Atmospheric Boundary Layer (definition, similarity theory, ...)
    * Formulation of electromagnetic wave propagation in random media
    * impacts of turbulence on signal strength and angle of arrival
    * Heat and water transfers from surface to atmosphere.
    * Instrumentation and application
