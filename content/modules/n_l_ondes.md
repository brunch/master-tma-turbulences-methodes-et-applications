Title: UE Turbulence d’ondes et non linéarités 3ECTS (21h)
Date: 2021-01-25
Category:  Modules d'enseignement
lang: fr

Nicolas Mordant, Pierre Augier, Mathieu Gibert

![Image]({static}../films/vagues3nosound.mp4)
_courtesy N.M._

#Résumé : (en construction)


#Répartition CM/TD:
environ 10 CM/TD (21h) et 1 TP (6h)

#Plan :
##Partie 1 : généralités sur la turbulence et les ondes non linéaires
- turbulence hydrodynamique: spectre de Kolmogorov 1941, casdade directe/inverse
- ondes linéaires, non linéaires, interaction triadiques, résonances, développement asymptotique, exemples d'ondes dans les fluides et les solides

##Partie 2 :  turbulence faible d'ondes: obtention de l'équation cinétique
- équation d'onde non linéaire, représentation d'interaction
- développement faiblement non linéaire
- condition initiale de phase aléatoire
- équation cinétique de l'évolution du spectre des ondes

##Partie 3 Propriétés de l'équation cinétique et ses solutions
- quantités conservées
- solutions stationnaires: équipartition et cascades
- analyse dimensionnelle
- direction des cascades
- solutions non stationnaires autosimilaires

##Partie 4 : quand la turbulence d'onde n'est plus faible
- échelles de temps
- rupture des hypothèses de la turbulence faible (forte non linéarité, taille finie)

##Partie 5: cas de la turbulence stratifiée
- stratification en densité, approximation de Boussinesq
- turbulence en présence de stratification, régimes possibles en fonction des nombres de Froude et de Reynolds

##Partie 6: solitons, turbulence intégrable
- ondes non linéaires localisées, soliton
- interaction de deux solitons
- gaz de solitons, turbulence intégrable, équation cinétique pour les gaz de solitons

## TP:
* TPs en labo sur la turbulence d'onde par exemple sur les plaques vibrées/gongs, ondes à la surface de l'eau (solitons)
* TPs numériques
