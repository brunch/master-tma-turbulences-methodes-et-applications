Title: French/English Understanding 3ECTS (24h)
Date: 2021-11-25 
Category:  Teaching modules 
lang: en


Penelope Moffatt (Univ Poitiers), Sylvain Perraud (SDL),
Adeline Braibant (CUEF FLE), Signe Seidelin (UFR PHITEM)

#Summary:

oral and written comprehension, bilingualism, interculturalism, bibliographic work

#Distribution classes/practicals:
approximately 2 lectures/classes (3h), 10 practicals (15h), 4 personal work sessions (12h)
Taught in both French and English

#Plan:
To make students autonomous understanding scientific presentations in English/French, and at analysing documents/articles in English/French. Implementing scientific cross-talk in the main language (the one better mastered). This will be based on articles/books and recordings of seminars on the topic of turbulence. The module will be co-supervised with language teachers (SDL, CUEF) and turbulence teachers.

* reading comprehension on turbulence, based on scientific reference articles (e.g. [Experiments in Fluids](https://link.springer.com/article/10.1007/BF00206540), [Journal of the Aeronautical Sciences](https://arc.aiaa.org/doi/10.2514/8.2793))
* oral comprehension on turbulence, based on conferences and seminars (e.g. [Cambridge Univ Press](https://www.youtube.com/watch?v=X9bnISZxE3Q&list=PLTK8KRW19hUV8DueNX1nVh3xVj7MrW-6m&index=10), [Batchelor Prize Lecture](https://www.youtube.com/watch?v=2ROJXcyCgaQ&list=PLTK8KRW19hUV8DueNX1nVh3xVj7MrW-6m&index=1))
* bilingual student tandems, teacher tandems and group projects 
* bilingual reading lists on core modules 
* conception and presentation of posters on the theme of one of the course modules with bilingual question and answer sessions
