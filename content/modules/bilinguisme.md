Title: UE Compréhension Anglais/Français 3ECTS (24h)
Date: 2021-01-25 
Category:  Modules d'enseignement 
lang: fr


Penelope Moffatt (Univ Poitiers), Sylvain Perraud (SDL),
Adeline Braibant (CUEF FLE), Signe Seidelin (UFR PHITEM)

#Résumé :

compréhension orale et écrite, bilinguisme, interculturalisme, travail bibliographique

#Répartition CM/TD:
environ 10 CM/TD (15h) et 6 TP (9h)
Enseigné à la fois en français et en anglais

#Plan :
rendre les étudiants autonomes pour comprendre une présentation scientifique en anglais/français, et analyser un document/article  en  anglais/français. Mettre en œuvre des dialogues scientifiques croisés dans la langue principale (la mieux maitrisée). On s’appuiera sur des articles/livres et des enregistrements de séminaires sur le thème de la turbulence. Le module sera coencadré avec des enseignants de langue (SDL, CUEF) et des enseignants spécialistes de la turbulence.

* compréhension écrite sur la turbulence, à partir d'articles scientifiques de référence (e.g. [Experiments in Fluids](https://link.springer.com/article/10.1007/BF00206540), [Journal of the aeronautical Sciences](https://arc.aiaa.org/doi/10.2514/8.2793))
* compréhension orale sur la turbulence, à partir de conférences ou séminaires (e.g. [Cambridge Univ Press](https://www.youtube.com/watch?v=X9bnISZxE3Q&list=PLTK8KRW19hUV8DueNX1nVh3xVj7MrW-6m&index=10), [Batchelor Prize Lecture](https://www.youtube.com/watch?v=2ROJXcyCgaQ&list=PLTK8KRW19hUV8DueNX1nVh3xVj7MrW-6m&index=1))
* projet bilinguisme d'étudiants en tandem, d'enseignants en tandem et projets de groupe 
* sujets bilingues bibliographiques sur les modules de tronc commun 
* élaboration et présentation de posters sur le thème d'un des modules du parcours avec séances de questions/réponses bilingues

