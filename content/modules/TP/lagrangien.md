Title: TP suivi lagrangien
Date: 2022-08-30
Category: TP Turbulence
lang: fr, en


Objectives :

* Learn how to use an imaging system with a high-speed camera. 

* Learn how to use matlab for image processing.

* Measure the size of bubbles.

* Find the trajectories of bubbles with different sizes.

* Study the different trajectories as a function of different parameters.

![Image]({static}../../figures/TP_lagrangien.png)
