Title: TP turbulence compressible et cavitation
Date: 2022-02-13
Category: TP Turbulence 
lang: fr, en

* Titre : Analyse d’écoulement cavitant autour d’un profil NACA

* Contenu :

Présentation du phénomène de cavitation

Ananlyse par visualisations de la dynamique d’une poche de cavitation sur profil (mise en place de cartographie de type de cavitation en function de paramètres hydrodynamiques)

Analyse par PIV du champ de Vitesse autour d’un profil

Analyse de l’influence de la cavitation sur l’écoulement instationnaire

Mise en evidence des effets de compressibilité sur la turbulence 

![Image]({attach}../../figures/TP_cavitation.png)
