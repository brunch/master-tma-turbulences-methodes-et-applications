Title: TP turbulence diphasique
Date: 2022-02-13
Category: TP Turbulence 
lang: fr, en

* Title: Bubble columns 

* Context

Bubble columns are reactors where a gas phase is injected as bubbles
which are dispersed at the bottom of an initially stagnant liquid
column. Although these systems are widely used in industry, their
hydrodynamic is still poorly known. Their experimental study is
particularly challenging as they involve two (or more) phases with large
levels of turbulence on an opaque flow

* Objectives

This lab session consists on the study of the transition to the
heterogenous regime (see the videos, where the transition is observed
from left to right), characterized by the presence of mesoscales and
high turbulence. For that we will study the bubbles concentration, size
and velocity using a state-of-the-art technique: Doppler optical probes

* Keywords

Multiphase flows / reactors, experimental fluid dynamics, opaque flows

![Image]({attach}../../figures/bubble_columns.png)
