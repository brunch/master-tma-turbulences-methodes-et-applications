Title: TP turbulence d'onde
Date: 2022-02-13
Category: TP Turbulence
lang: fr, en

* Titre : Canal à houle : Turbulence d'ondes

* Résumé : Des expériences seront réalisées dans le canal à houle de 36 m du LEGI. Elles consistent à actionner un batteur piston pour produire des ondes de surface en eau peu profonde, libres de se propager sur un fond horizontal et de se réfléchir sur une paroi verticale en bout de canal. L'élévation de surface libre est enregistrée sur une portion de 16 m avec 8 caméras permettant d'obtenir une résolution sub-millimétrique. Différents forçages seront réalisés pour observer le cas d'ondes stationnaires, de solitons isolés et de gaz denses de solitons. Ce dernier état peut être compris comme de la turbulence intégrable, au sens ou cet état désordonné conserve masse, quantité de mouvement et énergie. 

Les mesures seront ensuite analysées grâce à la transformé de Fourier 2D, permettant de décomposer les ondes se propageant dans chaque sens. La signature spectrale des solitons sera discutée. Les distributions d'élévation de surface libre et de hauteurs de crêtes pourront être comparées aux théories classiques de type Gauss ou Rayleigh. 

![Image]({attach}../../figures/canal_gaz.jpg)
