Title: UE Methodes Expérimentales avancées en turbulence 3 ECTS (24h)
Date: 2021-01-17
Category: Modules d'enseignement 
lang: fr


#Résumé :
3 TP de turbulence au choix par groupe de 3 en application des modules Turbulence et processus:
chacun 3h de TP suivi de 3h de traitement de données.
1 TP démonstration dans le cadre d'une expérience dédiée à la recherche.

#Répartition CM/TD:
environ 3h CM, 3\*3h TP, 4\*3h TD

#Plan (indicatif) :

* TP 1 [soufflerie aérodynamique : ]({filename}TP/sillage.md)  sillage turbulent

* TP 2a [soufflerie grille active : ]({filename}TP/thi.md) Turbulence Homogène Isotrope

* TP 2b data [soufflerie grille Modane : ]({filename}TP/thi_modane.md)  Turbulence à très haut nombre de reynolds (2023)

* TP 3 [soufflerie axisymétrique libre : ]({filename}TP/jet.md) jet turbulent (2023)

* TP 4a démonstration [plaque tournante Coriolis LEGI : ]({filename}TP/Coriolis.md) Turbulence stratifiée en rotation

* TP 4b plaque tournante ISTERRE (2023)

* TP 5 démonstration [canal à houle LEGI : ]({filename}TP/houle.md) Turbulence d'ondes

* TP 6 [plaques vibrées/gongs : ]({filename}TP/plaque.md) Turbulence d'ondes

* TP 7 [canal à pente variable : ]({filename}TP/pente.md) couche limite turbulente rugueuse (2023)

* TP 8 [canal à écoulement gravitaire stratifié : ]({filename}TP/gravitaire.md) turbulence stratifiée sur pente

* TP 9 [couche limite atmosphérique en montagne : ]({filename}TP/cla.md) couche limite atmosphérique turbulente

* TP 10 démonstration [couche de mélange avec cavitation : ]({filename}TP/cavitation.md) Turbulence compressible

* TP 11 [colonne à bulle : ]({filename}TP/colonne_bulle.md)  Turbulence diphasique

* TP 12 [suivi lagrangien : ]({filename}TP/lagrangien.md) Turbulence diphasique

* TP 13 démonstration [atomisation de jet : ]({filename}TP/atomisation.md)  Turbulence diphasique

* TP 14 démonstration turbulence Helium : Turbulence superfluide (2023)


