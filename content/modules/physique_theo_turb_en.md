Title: Theoretical physics of turbulence 3ECTS (24h)
Date: 2021-01-20
Category:  Teaching modules
lang: en

Léonie Canet, Vincent Rossetto

![Image]({static}../figures/kolmogorov_spectrum.png)
_courtesy N.M. AJP publication_

#Abstract :

The Navier-Stokes equations remain unsolved to this day, and predicting the statistical properties of turbulent flows is a major challenge for many applications. Numerous theoretical approaches have been developed to both model and compute the behaviour of turbulent flows beyond the 1941 Kolmogorov theory. This course will present several of these approaches, from the multi-fractal formalism to the renormalization group methods, focusing on the ideal case of isotropic homogeneous turbulence.

#Distribution CM/TD:
 approximately 12 CM (18h) and 4 TD (6h) taught mainly in french

#Contents (indicative) :

##PART A - advanced phenomenology of turbulence
1. _Experimental and numerical observations of intermittency_:
manifestations of intermittency, universal properties, main deviations from the predictions of the Kolmogorov 1941 theory
2. _The multi-fractal formalism_:
reminder of fractals, scale invariance, need to account for multi-scale character, Kolmogorov 62 theory, multi-fractal models.

##PART B - statistical description of turbulence
1. _Spontaneous stochasticity_:
notion of spontaneous stochasticity for an ordinary differential equation, spontaneous stochasticity in simplified models of turbulence, justification of statistical approaches
2. _Some simplified models of turbulence_:
Lorenz model and strange attractor, layered models, Burgers equation
3. _The renormalization group_:
Field theory for Navier-Stokes, symmetries, Karman-Howarth from symmetries, Wilson's renormalisation group.
