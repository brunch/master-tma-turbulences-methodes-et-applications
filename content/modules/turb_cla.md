Title: UE Turbulence en couche limite atmosphérique 3ECTS (24h)
Date: 2021-11-25
Category:  Modules d'enseignement
lang: fr

Christophe Brun, Jean-Martial Cohard

![Image]({static}../figures/ballon.png)
_courtesy C.B. BLM publication_

# Résumé :

former aux bases de la turbulence dans la couche limite atmosphérique.

# Répartition CM/TD:

environ 4 CM (6h), 4 TD (6h), 6 CM/TD (9h) et 1 TP (3h)

# Plan :

## Partie 1 : Turbulence en couche limite atmosphérique (en construction)

1. Turbulence
    * bilan d'énergie cinétique turbulente TKE et d'énergie potentielle turbulente TPE
    * bilan de flux de quantité de mouvement et de flux de chaleur sensible
    * anisotropie de la turbulence
    * spectres d'énergie

2. CLA neutre, stable et instable
    * stratification thermique
    * équilibre flottabilité et cisaillement mécanique
    * échelle de longueur d'Obukhov
    * correction de la loi logarithmique de vitesse et de température


##Partie 2 : Turbulence optique (en construction)

1. Propagation des ondes électromagnétiques dans les milieux aléatoires
    * Paramètre de structure dans la couche limite atmosphérique (définition, théorie de la similarité, ...)
    * Formulation de la propagation des ondes électromagnétiques dans les milieux aléatoires.
    * Impacts de la turbulence sur la force du signal et l'angle d'arrivée.
    * Transferts de chaleur et d'eau de la surface à l'atmosphère.
    * Instrumentation et application
