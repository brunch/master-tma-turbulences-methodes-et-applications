Title: Numerical simulation of air-entrainment in plunging jets and hydraulic jumps
Date: 2022-10-11
Category:  Stages M2
lang: fr, en

## Supervisor :

- Dr. Julien CHAUCHAT (LEGI/UGA/GINP/CNRS)
- Dr. Olivier BERTRAND (ARTELIA)
- Dr. Thibault OUDART (ARTELIA)
- Dr. Ing. Cyrille BONAMY (LEGI/CNRS/UGA/GINP)

## Contact :

Eduard.Puig-Montella@univ-grenoble-alpes.fr

## Lab :

équipe MEIGE / LEGI Grenoble
&
ARTELIA / Echirolles

## Date

6 months from February 2023

## Key Words :

Fluid mechanics, Soil mechanics, experience in programming

##Gratification : 

approx. 600 €/month

## Context :

The ability to predict air entrainment is a major engineering problem to design efficient and safe hydraulic structures. The physical complexity of the processes at work and their multi-scale nature, ranging from the smallest air bubble sizes (few tens of microns) to the most energetic eddy sizes (meter scale), makes it difficult for numerical models to accurately predict these phenomena. The objective of this project is to improve the numerical modeling of turbulent flows of air and water mixtures around renewable energies including hydroelectricity. These flows are found for example in the dissipation of the overflowing weirs on spillways, during the entrainment of air in the water intakes in river or in the suction chambers of pumps. In the context of this internship, the problem of air entrainment will investigated in connection with the concerns of the engineering company ARTELIA. The long-term goal of is to develop more accurate numerical models that integrate complex physical processes to improve the predictive capacity of engineering models.

## Internship:

The objective of the internship is to contribute to the development of a numerical model in openFOAM to simulate air-entrainment. The evaluation will be performed on different configurations: plunging jets, hydraulic jumps and pumping stations, for which experimental data are available. OpenFOAM proposes different theoretical two-phase flow and turbulence models and their ability to simulate air entrainment problems and to calculate energy dissipation will be analyzed. The choice of the best model requires experimental validation.
In a first step, simulations will be performed on the plunging jet configuration of Chanson et al. (2004). High resolution Large Eddy Simulation (LES) have already been performed at LEGI and the goal is to develop and
evaluate sub-grid scale models for air-entrainment in LES. Ultimately, based on the knowledge gained with the LES, we would like to further develop a Reynolds-Averaged model for this problem.
In a second step, other configurations will investigated numerically either the hydraulic jump configuration using the experimental data from Murzyn et al (2003), bearing chute using data from ARTELIA lab (see left figure) or pumping station using data from ARTELIA lab. These simulations will allow to evaluate the developed model on a different configuration. The sensitivity to the mesh size, turbulence models and air- entrainment models will be tested.


##Required Qualifications, Skills and Experience

- Theoretical: fluid mechanics, turbulence, multi-phase flows;
- Numerical: CFD, an experience with openFOAM will be appreciated but is not mandatory, experience with Python and/or C++ is an advantage.

##References
  
Murzyn, F., Mouazé, D. and Chaplin, J. (2007) Air-Water Interface Dynamic and Free Surface Features in Hydraulic Jumps. Journal of Hydraulic Research, 45, 679-685. http://dx.doi.org/10.1080/00221686.2007.9521804
Chanson, H., Aoki, S., and Hoque, A. (2004). Physical modelling and similitude of air bubble entrainment at vertical circular plunging jets. Chemical Engineering Science, 59(4):747–758.

![Image]({static}../figures/bearing_chute.png)

Figure 1 : Model of a bearing chute (Versailles, France) - Scale: 1/15. Experiments carried out at ARTELIA laboratory. Source : ARTELIA web site Description

![Image]({static}../figures/jet_bath.png)

Figure 2 : Experimental (left) and Numerical simulations (right) of cylindrical jet impacting a bath,. Simulations performed at LEGI using openFOAM. Source : exp. Chanson et al. (2004)
