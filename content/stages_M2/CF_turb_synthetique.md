Title: Vers un forçage pour la turbulence synthétique inhomogène à divergence et hélicité nulles.
Date: 2022-11-21
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Christophe FRIESS (enseignant-chercheur M2P2) et Fabien DUVAL (ingénieur IRSN)

## Contact :

christophe.friess@univ-amu.fr

fabien.duval@irsn.fr

## Laboratoire :

IRSN Cadarache et/ou Laboratoire M2P2 à Marseille

## Date

4 à 6 mois, début en février / mars / avril 2023

## Mots Clés: 

Mécanique des fluides, Turbulence, Calcul scientifique

## Contexte :

La prévision du mélange turbulent d’esèces inflammables constitue la première phase d’évaluation des risques d’une explosion dont le coût et la qualité dépendent de l’approche adoptée pour la modélisation de la turbulence. On distingue deux types d’approche : l’approche statistique RANS (Reynolds-Averaged Navier-Stokes), qui consiste en une modélisation de toutes les échelles turbulentes, et l’approche LES (Simulation des Grandes Echelles) qui consiste en un calcul direct des grandes échelles. L’approche RANS présente l’avantage d’être rapide mais sa précision dépend des caractéristiques du modèle de turbulence alors que l’approche LES est bien plus prédictive mais nécessite des temps de calcul plus importants pouvant être rédhibitoires pour des installations de grande taille et des transitoires longs. Dans ce contexte, les méthodes hybrides RANS/LES constituent une alternative à fort potentiel mais posent la difficulté de la génération de fluctuations turbulentes dans les zones de transition.

Le stage proposé s’inscrit dans le cadre d’une collaboration entre le M2P2 et l’IRSN. Le M2P2 est une unité mixte de recherche rattach ́ee à l’Université Aix-Marseille, à l’Institut des Sciences de l’Ingénierie et des Systèmes du CNRS (INSIS) et à Centrale Marseille. Le laboratoire est aussi membre de l’Institut Mécanique & Ingénierie et de la Fédération ECCOREV. Le laboratoire possède une position originale avec une recherche couvrant les domaines de la M écanique des Fluides Numérique et du Génie des Procédés. Cette activité est menée au sein de six équipes localisées sur le Technopôle de Château-Gombert à Marseille et l’Europôle de l’Arbois à Aix en Provence. De son côté, l’IRSN, Etablissement Public à caractère Industriel et Commercial (EPIC) est l’expert public national des risques nucléaires et radiologiques. L’IRSN concourt aux politiques publiques en matière de sûreté nucléaire et de protection de la santé et de l’environnement au regard des rayonnements ionisants. Organisme de recherche et d’expertise, il agit en concertation avec tous les acteurs concernés par ces politiques, tout en veillant à son indépendance de jugement.

## Stage :

Le but du stage proposé est d’améliorer une méthode de forçage volumique pour la génération de fluctuations turbulentes. Cette méthode, implantée dans le logiciel open-source CALIF3S développé à l’IRSN, consiste à enrichir le champ de vitesse résolu à partir d’une vitesse synthétique construite comme une représentation discète de modes de Fourier [1]. La figure 1 illustre le fonctionnement de ce forçage. La vitesse synthétique est par construction à divergence nulle pour une turbulence homoène et il s’agira d’étendre cette propriété au cas non-homoène. On s’intéressera égalementà l’hélicité artificielle introduite par le forçage età sa réduction.

## References

[1] J. Janin, F. Duval, C. Friess, and P. Sagaut. A new linear forcing method for isotropic turbulence with controlled integral length scale. Physics of Fluids, 33(4) :045127, 2021.

[2] Jérémie Janin. Volume forcing method based on a reconstruction-like procedure for a hybrid RANS/LES algebraic closure model. PhD thesis, Aix-Marseille Universit ́e, soutenance pr ́evue en janvier 2023.

![Image]({static}../figures/jet_synthetique.png)

Figure 1: Vitesse instantanée au sein d’un jet plan. L’image illustre l’introduction du for ̧cage générant des fluctuations turbulentes, à x = 6 diamètres de la buse d’injection [2]
