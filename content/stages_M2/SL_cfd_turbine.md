Title: Investigation of the Deep Part Load Behavior of a Francis Turbine via CFD.
Date: 2022-11-29
Category:  Stages M2
lang: fr, en

## Supervisor :

Sebastian Leguizamon

## Contact :

Sebastian.Leguizamon@ge.com

## Lab :

GE, General Electric
## Date

2023, 6 months 

## Keywords : 

Turbomachines, fluid mechanics and CFD,  Unix, Ansys CFX, Autogrid

## Context :

The integration of intermittent renewable energy sources (wind, solar, etc.) onto the electrical grid forces hydraulic machines to operate very far from their design point. In regimes of very low discharge the flow is complex and relatively poorly understood; few experimental measurements are generally available. However, we know that operating in these regimes is very damaging to the mechanical fatigue life of the runner, so a better understanding of the underlying phenomena is crucial.

## Internship :

The objective is to investigate via CFD the regime of deep part load known as speed no-load:
- First, the numerical methodology will be optimized by comparison to existing experimental measurements; it will then be validated on several test cases, which will also yield a quantification of the associated uncertainty.
-  Second, the flow fields will be analyzed to better understand the hydraulic phenomena at play, for instance by correlating them to the spectral signatures obtained experimentally. This point will require novel post- processing techniques to be able to identify the underlying structures to be analyzed, in the context of a flow that is quite complex and apparently stochastic.
- Third, runner design modifications will be explored to investigate their impact on the hydraulic phenomena and their fluctuation characteristics.

The Intern will join the CFD team that is in charge of, among other things, the development of simulation methodologies. He or she will also have the opportunity to work at the interface between the CFD team, the hydraulic design team, and the transversal team dedicated to the study of operational flexibility.

![Image]({static}../figures/turbine.png)
Figure 1: vortex sheding behind turbine blades by mean of CFD
