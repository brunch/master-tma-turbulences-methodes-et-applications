Title: Investigation of the stability limits of a linearized implicit scheme for turbulent incompressible flows.
Date: 2022-11-29
Category:  Stages M2
lang: fr, en

## Responsable de stage :

Thomas Berthelon (Ingénieur de recherche LEGI) et Guillaume Balarac (enseignant-chercheur E3)

## Contact:

thomas.berthelon@univ-grenoble-alpes.fr

guillaume.balarac@univ-grenoble-alpes.fr

## Lab:

LEGI/MoST Grenoble

## Date

5 mois, début en février 2023

## Keywords: 

Large Eddy Simulation, Stability, Time Integration, Incompressible Flow

## Context:

The strong increase in computing power observed in recent years has made it possible to use Large Eddy Simulations (LES) for industrial configurations. However, the restitution time remains too high for a daily use in the design phases. In order to reduce this time, an implicit time integration scheme has been developed in the YALES2 library [1]. This allows the use of larger time steps than those imposed by the explicit time integration constraints, historically the most used in the LES of incompressible flows community. Nevertheless, the stability constraint of these new schemes is not yet well understood. In order to fully use the potential of these methods it is necessary to obtain criteria which ensure their stability.


## Internship:

The first step of the internship will consist of a literature review on the stability of time integration schemes. It will be followed by the investigation of the stability limit for various configurations, involving different types of unsteady flows (turbulent jet, turbulent boundary layer, laminar instability...). This step will allow the generation of an exhaustive numerical database to identify robust stability criteria. Machine learning tools could be used to take full advantage of this database.

## Expected knowledge: 

Numerical Modelling, Fluid Mechanics

## References

[1] V. Moureau, P. Domingo, and L. Vervisch. Design of a massively parallel CFD code for complex geome- tries. Comptes Rendus Mécanique, 339(2):141–148, 2011.
