Title: Numerical analysis of flow and morphological patterns around moving objects
Date: 2022-10-11
Category:  Stages M2
lang: fr, en

## Supervisor :

Eduard PUIG MONTELLA, Julien CHAUCHAT and Cyrille BONAMY

## Contact :

Eduard.Puig-Montella@univ-grenoble-alpes.fr

## Lab :

équipe MEIGE / LEGI Grenoble

## Date

February to July 2023

## Key Words :

Fluid mechanics, Soil mechanics, experience in programming

##Gratification : 

Approximately 700€/month

## Context :

Sediment resuspension and erosion are important phenomena in many environmental and industrial processes. It is well-established that particle resuspension is related to turbulent flows, however, the understanding of the underlying physics is still weak. In order to gain more insight, Munro et al. (2009) investigated the sediment resuspension induced by a vortex ring interacting with a sediment layer using particle image velocimetry (see figure 1a). Similarly, Steiner et al (2023) is currently studying the flow field and onset of erosion induced by an oscillating disc (see figure 1b). Numerical studies, on the other hand, are scarce or non-existing. This opens up a new opportunity to have access to information experiments are not able to measure. Another classical example of erosion enhanced by fluid-sediment-structure interactions is the scour around a cylinder/pipe placed on a granular bed (see figure 1d). Sediment transport driven by currents or waves can cause scour or local fluidization leading to pipeline failures or the burial of the object .


## Internship:

The goal of this master project is to reproduce the experimental configurations of Steiner (2023) using an Eulerian two-phase flow solver implemented in OpenFoam (SedFoam, Chauchat et al., 2017). The candidate will analyze and compare the evolution of the bedforms and the flow patterns with Steiner (2023) results. A second objective is to study the trajectory and behavior of a cylinder on a sediment layer under the effect of an oscillatory flow following the configuration of Tsai (2022), and eventually, perform a sensitivity analysis considering different burial depths, diameters and sediment and fluid properties.

## Requirements: 

The candidate should have a good knowledge of fluid and solid mechanics. Programming skills in Python and/or C++ is recommended. The position is opened to candidates having a bachelor degree.

##References
  
Chauchat, J., Cheng, Z., Nagel, T., Bonamy, C., & Hsu, T. J. (2017). SedFoam-2.0: a 3-D two-phase flow numerical model for sediment transport. Geoscientific Model Development, 10(12).
Munro, R. J., Bethke, N., & Dalziel, S. B. (2009). Sediment resuspension and erosion by vortex rings. Physics of Fluids, 21(4), Steiner, J (2023). Rôle des tourbillons et fluctuations de vitesse dans l'érosion.
Tsai, Benjamin, et al. (2022) An Eulerian two-phase flow model investigation on scour onset and backfill of a 2D pipeline.

![Image]({static}../figures/vortex_ring.png)

Fig. 1: Structure of the double vortex rings interacting with a sediment bed (Munro et al. (2009)) (top). Flow patterns around an oscillating disc (Steiner (2023)) (bottom). 

![Image]({static}../figures/bed_form.png)

Fig. 2: Evolution of the velocity field and bedforms induced by an oscillatory disc using SedFoam (top). Pipe burial and flow field reproduced numerically with SedFoam by Tsai (2022) (bottom).
