Title: Data analysis of experiments on rotating downslope gravity currents
Date: 2022-02-16
Category:  Stages M1
lang: fr, en

## Supervisor :
Eletta Negretti
## Contact :
eletta.negretti@legi.cnrs.fr
## Lab :
LEGI Grenoble
## Date :
not available in 2022
## Key Words :
gravity currents, stratified/rotating turbulence, topography, boundary layers
## Contexte :
When buoyancy driven flows encounter topography, a downslope dense current, also called gravity current, is created. Gravity currents (GCs) play an important role for the transport of water masses in the oceans. Their exchange involve boundary layers (BLs), high shears and density gradients, instabilities and generation of sub-mesoscale vortices at [km] scale. The resulting mixing influences the final stabilisation depth of the water mass, which controls the whole convection process (thermohaline circulation). These flows however, occur at such small scales and are so rapid that a full resolution of their dynamics is out of reach for numerical models. Consequently, they need to be correctly represented in general ocean circulation models (OCMs) (see Danabasoglu et al (2010)).
## Project :
The main goal of this master project is to process existing experimental data of experiments performed at the Coriolis Rotating Platform, realized using saline solutions to generate GCs injected at several points equispaced on the platform’s circonference at the top of an inclined boundary, shaped as an inversed cone. Velocities were measured using PIV and the density using conductivity probes. Both PIV data and the conductivity probes data have to be analyzed for the different 12 experiments that has been performed. The outcome of the internship is part of a larger project (TUBE) in order to test and calibrate the developed numerical code CROCO under non-hydrostatic conditions in collaboration with the SHOM laboratory (Brest, AID).
More on the project on this website :http://www.legi.grenoble-inp.fr/web//spip.php?article1441

![Image]({static}../figures/coriolis.png)
Top view of an experiment of an axisymmetric rotating gravity current with the injected saline solutions highlighted by the fluorescent dye. The salty flow descents the slope adjusting to the Coriolis force and finally intrudes into the two-layered ambient flow creating small scale and dipole structures as evident from the image.
