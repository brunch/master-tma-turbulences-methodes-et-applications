Title: Turbulent boundary stress in the lunar core
Date: 2022-02-11
Category:  Stages M1
lang: fr, en

## Supervisor :

David Cébron

## Contact :

david.cebron@univ-grenoble-alpes.fr

## Lab :

ISTerre Grenoble

## Date :

whenever in the Spring of 2022

## Key Words :

numerics; boundary turbulence, planetary core

## Context :

In the frame of the ERC project THEIA, we aim at revisiting the boundary
coupling between a planetary liquid core (e.g. the one of the Earth or the Moon) and the
surrounding planetary mantle. Indeed, geodetic observations constrain this coupling to
values which are difficult to reconcile with our current flow dynamics models. In particular,
two ingredients are quite badly known: the boundary turbulence and the topographic
effects on the flow and on the boundary stress in such planetary cores. Before combining
these two key ingredients, which is our long-term goal, we first aim at caracterizing each
of them separately in the relevant geometry, when the flow is dominated by rotation.

## Project :

In this internship, we will consider the current lunar liquid core, where we may
expect no leading order buoyancy and magnetic effects on the boundary layer. Using the
in-house spectral code XSHELLS developed by N. Schaeffer, we will simulate Ekman layer
instabilities in a rotating sphere, in order to trigger a sustained boundary layer turbulence.
Beyond our interest on the instability onset, the turbulent stress will be compared to the
laminar one (which is known theoretically), to recent plane layer simulations (Buffett
2021), and to a turbulent model developed for plane Ekman layer at LEGI, by Sous &
Sommeria (2013). While we do not expect to reach the highly turbulent regime where this
latter model is expected to be valid, we aim at bridging the gap between the well known
laminar models and this asymptotic regime.

![Image]({static}../figures/eckman_layer_sphere.png)
Ekman layer, which is laminar (left), and then destabilized into longitudinal rollls (center),
ending in turbulence (right) in a rotating sphere in libration (Sauret et al., JFM 2013)
