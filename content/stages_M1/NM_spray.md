Title: The role of gas turbulence in gas-assisted liquid atomization and sprays
Date: 2022-02-08
Category:  Stages M1
lang: fr, en

## Supervisor :

Nathanaël Machicoane (équipe EDT)

## Contact :

nathanael.machicoane@univ-grenoble-alpes.fr

## Lab :

LEGI Grenoble

## Date :

2 months starting in the Spring of 2022

## Key Words :

experimental; turbulent two-phase flows; data and image processing

## Context :

Gas-assisted atomization is a widely used process used in industry (manufacturing, pharmaceutical, agricultural…) to produce a high-quality spray. A high-speed gas accelerates and destabilizes a low-momentum liquid jet, to successively break it into a cloud of fine droplets, namely a spray. In the context of propulsion applications, the gas enters the jet engine turbines and becomes highly turbulent, with large velocity fluctuations as well as angular momentum, referred to as swirl. Despite their widespread use in industry, the modeling of such coaxial atomizer remains unsatisfactory due to the lack of a complete mechanistical understanding of the underlying physical phenomena describing the interfacial instabilities and the break-up processes. The design and exploitation of these systems rely on empirical approaches, extracted on uncomplete parameter ranges with respect to the wide nature of the applications, and with no thorough description of the effect of swirl and turbulence in the gas co-flow.

## Project :

The goal is to investigate different levels of turbulent fluctuations in the gas co-flow, and how turbulence affects the spray formation mechanisms. These include the initial interfacial and large-scale instabilities of the liquid jet and the subsequent break-up events into droplets. The study will require the combination of a set of available measurements techniques and the development of analysis tools. Hotwire anemometry and/or particle image velocity will be used to characterize the gas turbulence, while high spatial and temporal resolution imaging will help study the spray. The experimental techniques used will rely on high-speed imaging cameras, optical probes, and the associated processing techniques.

![Image]({static}../figures/spray.png)
Left: High-speed back-lit imaging of the spray near-field, for a coaxial atomizer with laminar liquid injection and slightly turbulent gas co-flow.
Right: High spatial and temporal resolution synchrotron X-ray radiography for the same condition.
The liquid jet diameter is 2.5 mm for both pictures.
