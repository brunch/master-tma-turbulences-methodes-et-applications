Title: Spatio-temporal analysis of strongly stratified turbulence forced in rotational modes
Date: 2022-03-02
Category:  Stages M1
lang: fr, en

## Supervisor :

Pierre Augier (équipe MEIGE)

## Contact :

pierre.augier@univ-grenoble-alpes.fr

## Lab :

LEGI Grenoble

## Date :

2 months starting in the Spring of 2022

## Key Words :

numerical; density stratification; turbulence; spatio-temporal spectra

## Context :

Strongly stratified turbulence is a dynamical regime that could play a key role
in the atmosphere and the oceans. It can be observed for flows strongly
influenced by a stable density stratification at very large Reynolds number. It
is associated with an anisotropic downscale energy cascade. The most standard
way to produce this regime in numerical simulations is to inject energy in
vortical modes (associated with vertical vorticity). Spatio-temporal spectra
have been used to study the dynamics of stratified flows forced in wavy modes,
for which internal waves can dominate the flow leading to wave turbulence.
However, these tools have not yet been seriously applied to more standard
strongly stratified turbulence.

## Project :

At LEGI, we develop and use the Computational Fluid Dynamics (CFD) framework
[Fluidsim](https://fluidsim.readthedocs.io). We now have a very good numerical
setup to produce and study stratified turbulence forced in wavy or vortical
modes
([fluidsim.util.scripts.turb_trandom_anisotropic](https://fluidsim.readthedocs.io/en/latest/generated/fluidsim.util.scripts.turb_trandom_anisotropic.html)).
In particular, computing and plotting temporal and spatio-temporal spectra is
now very easy. Turbulence forced in wavy modes is investigated in a
collaboration with Giorgio Krstulovic and Vincent Labarre (Observatoire de la
Côte d'Azur). We propose to focus during this internship on the case with
vortical forcing. Does strongly stratified turbulence lead to temporal spectra
similar to the one observed in geophysical fluids? The student would have the
opportunity to run simulations on a national cluster and to enjoy the power of
Fluidsim and of the scientific Python ecosystem.

![Image]({static}../figures/fig_ns3d.strat_toro_640x640x160.png)
Horizontal cross-section of buoyancy for strongly stratified turbulence forced
by large vortices at moderate buoyancy Reynolds number (resolution:
640x640x160).
